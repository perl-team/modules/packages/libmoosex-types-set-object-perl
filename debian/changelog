libmoosex-types-set-object-perl (0.05-3) unstable; urgency=medium

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!
  * Remove Ryan Niebur from Uploaders. Thanks for your work!

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

  [ Debian Janitor ]
  * Bump debhelper from old 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Remove obsolete fields Contact, Name from debian/upstream/metadata
    (already present in machine-readable debian/copyright).

 -- Jelmer Vernooĳ <jelmer@debian.org>  Thu, 16 Jun 2022 15:02:08 +0100

libmoosex-types-set-object-perl (0.05-2.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 14 Jun 2022 14:02:15 +0200

libmoosex-types-set-object-perl (0.05-2) unstable; urgency=medium

  * Team upload

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ Damyan Ivanov ]
  * Declare conformance with Policy 3.9.7
  * Swap the order of the alternative build-dependency on Test-Simple

 -- Damyan Ivanov <dmn@debian.org>  Mon, 22 Feb 2016 08:13:09 +0000

libmoosex-types-set-object-perl (0.05-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Install CONTRIBUTING file.
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Update Test::use::ok build dependency.

  [ Florian Schlichting ]
  * Add debian/upstream/metadata
  * Import upstream version 0.05
  * Update years of packaging copyright
  * Declare compliance with Debian Policy 3.9.6
  * Mark package autopkgtest-able
  * Bump debhelper dependency to minimum version required by
    Module::Build::Tiny

 -- Florian Schlichting <fsfs@debian.org>  Fri, 21 Aug 2015 14:20:56 +0200

libmoosex-types-set-object-perl (0.04-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ Axel Beckert ]
  * debian/copyright: migrate pre-1.0 format to 1.0 using "cme fix dpkg-
    copyright"

  [ Florian Schlichting ]
  * Import Upstream version 0.04
  * Add build-dependency on Module::Build::Tiny
  * Update year of upstream copyright
  * Bump dh compatibility to level 8 (no changes necessary)
  * Declare compliance with Debian Policy 3.9.5
  * Add myself to uploaders and copyright

 -- Florian Schlichting <fsfs@debian.org>  Mon, 20 Jan 2014 23:16:55 +0100

libmoosex-types-set-object-perl (0.03-1) unstable; urgency=low

  [ Nathan Handler ]
  * debian/watch: Update to ignore development releases.

  [ Salvatore Bonaccorso ]
  * debian/control: Changed: Replace versioned (build-)dependency on
    perl (>= 5.6.0-{12,16}) with an unversioned dependency on perl (as
    permitted by Debian Policy 3.8.3).

  [ Ryan Niebur ]
  * Update jawnsy's email address
  * Update ryan52's email address

  [ Ansgar Burchardt ]
  * New upstream release.
  * Build-dep on libtest-fatal-perl instead of libtest-exception-perl.
  * Use source format 3.0 (quilt).
  * debian/copyright: Update for new upstream release, formatting changes.
  * Bump Standards-Version to 3.9.1.
  * Add myself to Uploaders.

 -- Ansgar Burchardt <ansgar@debian.org>  Sun, 05 Dec 2010 12:21:10 +0100

libmoosex-types-set-object-perl (0.02-1) unstable; urgency=low

  * Initial Release. (Closes: #529240)

 -- Jonathan Yu <frequency@cpan.org>  Mon, 18 May 2009 22:19:20 -0400
